import os
os.chdir("C:\\Users\\Administrator\\Downloads\\")

import spacy
 
from spacy import displacy

import tensorflow as tf
 
import pandas as pd 

import numpy as np
 
import re

import nltk

nltk.download('stopwords')
 
#from spacy import nl_core_news_sm
 
import unicodedata
 
from nltk.tokenize.toktok import ToktokTokenizer

from bs4 import BeautifulSoup

myfile = open(u"Veiligheid.txt")
 
tekst = myfile.read()


def remove_accented_chars(text):
    text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('utf-8', 'ignore')
    return text

def remove_special_characters(text, remove_digits=False):
    pattern = r'[^a-zA-z0-9\s]' if not remove_digits else r'[^a-zA-z\s]'
    text = re.sub(pattern, '', text)
    return text

tokenizer = ToktokTokenizer()
 
stopword_list = nltk.corpus.stopwords.words('dutch')
 
def remove_stopwords(text, is_lower_case=False):
    tokens = tokenizer.tokenize(text)
    tokens = [token.strip() for token in tokens]
    if is_lower_case:
        filtered_tokens = [token for token in tokens if token not in stopword_list]
    else:
        filtered_tokens = [token for token in tokens if token.lower() not in stopword_list]
    filtered_text = ' '.join(filtered_tokens)
    return filtered_text

def normaliseren_tekst(corpus, accented_char_removal=True, special_char_removal=True,
    stopword_removal=True):
        genormaliseerde_tekst = []
        for doc in corpus:
        # vervang karakters met accenten
            if accented_char_removal:
                doc = remove_accented_chars(doc)
        # verwijder speciale karakters
            if special_char_removal:
        # insert spaces between special characters to isolate them
                special_char_pattern = re.compile(r'([{.(-)!}])')
                doc = special_char_pattern.sub(" \\1 ", doc)
                doc = remove_special_characters(doc, remove_digits=bool())
        # verwijder stopwoorden
            if stopword_removal:
                doc = remove_stopwords(doc, is_lower_case=bool())
            
            genormaliseerde_tekst.append(doc)

        return genormaliseerde_tekst



tekst = normaliseren_tekst(tekst, accented_char_removal=True, special_char_removal=True, stopword_removal=True)

print(tekst)